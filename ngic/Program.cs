﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System.Collections.Generic;
using Serilog;
using ngi.implementation;
using ngbt.common;
using System;
using System.Reflection;
using System.Runtime.Versioning;

namespace ngic
{

    [SupportedOSPlatform("windows")]
    class Program
    {
        private readonly ILogger Log;
        private readonly Runtime Runtime = new Runtime();

        public Program()
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
        }

        static void Main(string[] args)
        {
            new Program().Run(args);
        }

        private delegate bool HandlerMethod(Installation description, List<string> nextMethodArgs);

        public bool Run(string[] args)
        {
            Log.Information("*** NGBT Installation Manager - Version {0} ***\r\nCopyright (C) 2020 by NG Branch Technology GmbH", Tools.Version);

            var installation = Installation.Detect(Log);
            if (installation != null)
            {
                Log.Information("Found existing installation:\r\n- {0}", installation);
            }
            else
            {
                Log.Information("Did not find an existing installation.");
            }

            HandlerMethod nextMethod = null;
            List<string> nextMethodArgs = new List<string>();

            foreach (string arg in args)
            {
                if (arg.Equals("/UNINSTALL", StringComparison.CurrentCultureIgnoreCase))
                {
                    nextMethod = Uninstall;
                }
                else if (arg.Equals("/BACKUP", StringComparison.CurrentCultureIgnoreCase))
                {
                    nextMethod = Backup;
                }
                else if (arg.Equals("/STARTUP", StringComparison.CurrentCultureIgnoreCase))
                {
                    Runtime.Startup = true;
                }
                else if (arg.Equals("/SHUTDOWN", StringComparison.CurrentCultureIgnoreCase))
                {
                    Runtime.Shutdown = true;
                }
                else if (arg.Equals("/RESTORE", StringComparison.CurrentCultureIgnoreCase))
                {
                    nextMethod = Restore;
                }
                else if (arg.Equals("/?") || arg.Equals("/HELP", StringComparison.CurrentCultureIgnoreCase))
                {
                    return Help();
                }
                else if (nextMethod != null)
                {
                    nextMethodArgs.Add(arg);
                }
            }
            if (nextMethod != null)
            {
                return nextMethod(installation, nextMethodArgs);
            }
            bool listInstallations = true;
            if(installation != null)
            {
                if (Runtime.Shutdown)
                {
                    installation.Shutdown();
                    listInstallations = false;
                }
                if (Runtime.Startup)
                {
                    installation.Startup();
                    listInstallations = false;
                }
            }
            if (listInstallations)
            {
                Log.Information("The following installations are available:");
                return Runtime.ListInstallations();
            }
            return true;
        }

        private bool Restore(Installation installation, List<string> args)
        {
            if (args.Count != 1)
            {
                Log.Information("Error: bad parameters for /RESTORE <name>");
                return false;
            }
            return Runtime.Restore(installation, args[0]);
        }

        private bool Uninstall(Installation installation, List<string> args)
        {
            return Runtime.Uninstall(installation);
        }

        private bool Backup(Installation installation, List<string> args)
        {
            if (installation == null)
            {
                Log.Information("ERROR, no installation detected => cannot backup");
                return false;
            }
            string filename = installation.Filename;
            if (args.Count == 1)
            {
                filename = args[0];
            }
            else if (args.Count == 2)
            {
                filename = args[0];
                installation.Name = args[1];
            }
            else if (args.Count == 3)
            {
                filename = args[0];
                installation.Name = args[1];
                installation.Description = args[1];
            }
            else if (args.Count > 0)
            {
                Log.Information("ERROR, bad parameters for /BACKUP. Please consult your non-existing manual.");
                return false;
            }
            return Runtime.Backup(installation, filename);
        }

        private bool Help()
        {
            Log.Information("USAGE: ngic [OPTIONS]");
            Log.Information("OPTIONS:");
            Log.Information("    /BACKUP <name> .... create a backup with a new name");
            Log.Information("    /RESTORE <name> ... installed named version");
            Log.Information("    /UNINSTALL ........ uninstall existing installation");
            Log.Information("    /STARTUP .......... startup existing installation");
            Log.Information("    /SHUTDOWN ......... shutdown existing installation");
            return true;
        }
    }
}
