# NGIC - The NGBT Installation Console - Version 1.2

NGIC is a command line tool that allows you to manage installations for NGBT software - mainly ProAKT and related products.

With the help of NGIC you can

- Create a single-file backup of an existing product installation, with a "human-readable" name and an optional description.
- Uninstall an existing product installation
- Restore a previously backed-up installation

Backups are stored in files with the extension .ngi - but they are really only dumb .zip files (i.e. you can view them with 7z). 

In effect, this allows you to do nice things such as:

- switch between two or more product versions
- copy product versions between virtual machines (or desktop computers)

## Installation

NGIC is a .NET 5.0 application.

First, assure you have the correct runtime installed. If in doubt, visit https://dotnet.microsoft.com/download and install it.

Next, extract all files from the NGI-1.0.0.zip to a folder on your filesystem (e.g. `C:\Tools\NGI`)

Open `C:\Tools\NGI\config\ngi.json` in your favourite editor. It should look something like this:

    {
        "InstallationFolders": [
            "C:\\Setups\\Installations",
            "C:\\Setups\\Release"
        ]
    }

This is a good time to think about where you want to store your installations. Chances are these folders do not exist, so point them to a folder that you want to use.

*Note*: If you are on VMWare, you can access shared folders like this:

    {
        "InstallationFolders": [
            ...
            "\\\\vmware-host\\shared folders\\share-name\installations"
        ]
    }

Save your changes and quit.

*Note*: The folders in this list must exist. If they do not, please create them first before continuing.

Open a command line *with administrative privileges*. If you don't choose *run as administrator*, ngic **WILL NOT RUN**.

Now, run NGIC. If you do not have ProAKT installed, your output should look like this:

    [2020-11-14 20:33:10.546 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:33:10.561 INF  NGBT Installation Console]
    Did not find an existing installation.

    [2020-11-14 20:33:10.562 INF  NGBT Installation Console]
    The following installations are available:

    [2020-11-14 20:33:10.567 INF  NGBT Installation Console]
    - Sorry, did not find any backup installations

Some comments: 

- NGIC always writes a logfile `ngic.log` that you can use to review all output.
- More installations will become available as you create backups.

If you *do* have ProAKT installed, instead, the output should look something like this:

    [2020-11-14 20:38:02.065 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:38:02.083 INF  NGBT Installation Console]
    Found existing installation:
    - ProAKT Standard 3.5.2.39 [Released 2020.09.25]

    [2020-11-14 20:38:02.084 INF  NGBT Installation Console]
    The following installations are available:

    [2020-11-14 20:38:02.089 INF  NGBT Installation Console]
    - Sorry, did not find any backup installations

Some comments:

- NGIC detects ProAKT (and ARCA Branch) installations automatically.
- NGIC also identifies the product version and release date.

One of the first things you can do with NGIC is to control the installation:

- `NGIC /STARTUP` will startup the installation (by calling `STARTAKT` or `TCRSTART`)
- `NGIC /SHUTDOWN` will shutdown the installation (by calling `STOPAKT` or `TCRSTOP`)

OK, that was great fun, but now for something more serious: let's create a backup. The full syntax is this:

    NGIC /BACKUP [filename] [name] [description]
    
*New in version 1.2*: If you omit the parameters (i.e. use /BACKUP alone), `NGIC` will attempt to determine the best match based on the installation detected. This works pretty well in the scenarios we've tested so far, so maybe this is the better choice for you ;)

Here is an example:

    C:\NGBT\NGBT\ngi\ngic\bin\Release\net5.0>ngic /BACKUP sbs-simulator "ProAKT 3.5.2.39 SBS Simulator"
    [2020-11-14 20:41:40.940 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:41:40.958 INF  NGBT Installation Console]
    Found existing installation:
    - ProAKT Standard 3.5.2.39 [Released 2020.09.25]

    [2020-11-14 20:41:40.963 INF  NGBT Installation Console]
    Creating backup C:\Setups\Installations\sbs-simulator.ngi

    ...

Quite a bit of line noise will follow - debugging information. (Don't worry, we're working on cleaning this up for the next release). The important bit is this: you've just now created a `C:\Setups\Installations\sbs-simulator.ngi` that is a full backup of your installation.

If you now run `ngic` again, your output will differ:

    C:\NGBT\NGBT\ngi\ngic\bin\Release\net5.0>ngic
    [2020-11-14 20:44:46.008 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:44:46.026 INF  NGBT Installation Console]
    Found existing installation:
    - ProAKT Standard 3.5.2.39 [Released 2020.09.25]

    [2020-11-14 20:44:46.027 INF  NGBT Installation Console]
    The following installations are available:

    [2020-11-14 20:44:46.060 INF  NGBT Installation Console]
    C:\Setups\Installations\sbs-simulator.ngi:
    - ProAKT 3.5.2.39 SBS Simulator [Released 2020.09.25]

So you see, you have an archive, and your human-readable name is visible. 

Next, let's uninstall this. Nothing easier:

    C:\NGBT\NGBT\ngi\ngic\bin\Release\net5.0>ngic /UNINSTALL
    [2020-11-14 20:45:54.251 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:45:54.269 INF  NGBT Installation Console]
    Found existing installation:
    - ProAKT Standard 3.5.2.39 [Released 2020.09.25]

    [2020-11-14 20:45:54.274 INF  NGBT Installation Console]
    Removing ProAKT Standard 3.5.2.39 [Released 2020.09.25]
    ...

If you see this warning at the end:

    [2020-11-14 20:45:55.674 INF  NGBT Installation Console]
    Warning: unable to completely remove existing installation: please check manually

It most likely means NGIC wasn't able to remove all registry entries. This is a known bug, and it is caused by security settings used by the *ProAKT Standard* MSI installer (in other words: it does not affect *ARCA Branch* installations). In this case, you *should* enter `regedit` and manually remove the registry.

Once you have removed the installation, run NGIC again:

    C:\NGBT\NGBT\ngi\ngic\bin\Release\net5.0>ngic
    [2020-11-14 20:49:10.804 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:49:10.819 INF  NGBT Installation Console]
    Did not find an existing installation.

    [2020-11-14 20:49:10.819 INF  NGBT Installation Console]
    The following installations are available:

    [2020-11-14 20:49:10.854 INF  NGBT Installation Console]
    C:\Setups\Installations\sbs-simulator.ngi:
    - ProAKT 3.5.2.39 SBS Simulator [Released 2020.09.25]

So you now have a backup, but no current installation. 

To restore the installation, use this command

    NGIC /RESTORE <filename>

Example:

    C:\NGBT\NGBT\ngi\ngic\bin\Release\net5.0>ngic /RESTORE sbs-simulator
    [2020-11-14 20:50:06.266 INF  NGBT Installation Console]
    *** NGBT Installation Manager - Version 1.0 ***
    Copyright (C) 2020 by NG Branch Technology GmbH

    [2020-11-14 20:50:06.280 INF  NGBT Installation Console]
    Did not find an existing installation.

    [2020-11-14 20:50:06.318 INF  NGBT Installation Console]
    Restoring this: ProAKT 3.5.2.39 SBS Simulator [Released 2020.09.25]

    ...

Some comments:

- You don't need to specify /UNINSTALL: if you have an existing installation detected by NGIC, it will remove it first, then install the backup.

## Portability

.ngi archives should be portable between machines - at least as you don't mix 32-bit and 64-bit ones. That means you can build up a "library" of .NGI archives and share them with others, so they can see your installation easily.

## Known Issues

1. MSI setups are not properly removed from the Windows installer database. The impact is this: Even if you have removed the installation with NGIC, Windows will still prompt you to remove it first via the MSI package, and only then allow you a new installation.

2. Handle *ProAKT Standard* registry permissions properly, so we can delete everything.

## Version History

### Version 1.1 [INTERNAL]

- Allow chosing an installation by "human-readable name" (not only filename)
- Support /BACKUP parameter autodetection
- Support for NG1 installations [WORK IN PROGRESS]

### Version 1.2

- Support detection of old ProAKT 3.1 installations
- Public availability of NGI UI

## Future Plans

- Cleanup logging, so it is more easy to understand what NGIC is doing


