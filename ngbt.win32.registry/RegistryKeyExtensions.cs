﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
 
using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Security.AccessControl;
using Microsoft.Win32;

namespace ngbt.win32.registry
{
    [SupportedOSPlatform("windows")]
    public static class RegistryKeyExtensions
    {
        public static string GetStringValue(this RegistryKey key, string name, string defaultValue = "")
        {
            var value = key.GetValue(name);
            if (value != null)
            {
                if (value is string actualValue)
                {
                    return actualValue;
                }
            }
            return defaultValue;
        }

        public static IEnumerable<string> DeleteKeyRecursive(this RegistryKey root, string keyPath)
        {
            var subkeys = new List<string>
            {
                keyPath
            };

            while (subkeys.Count > 0)
            {
                string currentKey = subkeys[0];
                bool removeAndKill = true;
                using (var key = root.OpenSubKey(currentKey, writable: false))
                {
                    if(key != null)
                    {
                        foreach (var name in key.GetSubKeyNames())
                        {
                            // subkeys must be removed before this can be removed
                            subkeys.Insert(0, $"{currentKey}\\{name}");
                            removeAndKill = false;
                        }
                    }
                    else
                    {
                        subkeys.RemoveAt(0);
                        removeAndKill = false;
                    }
                }
                if(removeAndKill)
                {
                    // cannot have any subkeys, so kill it now
                    subkeys.RemoveAt(0);
                    bool thisKeyMustBeYielded = false;
                    try
                    {
                        root.DeleteSubKey(currentKey);
                    }
                    catch(Exception)
                    {
                        //ngbt.common.Tools.DumpException(e, $"Failed to remove {currentKey}");
                        thisKeyMustBeYielded = true;
                    }
                    if(thisKeyMustBeYielded)
                    {
                        thisKeyMustBeYielded = false;
                        string user = Environment.UserDomainName + "\\" + Environment.UserName;

                        RegistrySecurity rs = new RegistrySecurity();

                        // Allow the current user to read and delete the key.
                        //
                        rs.AddAccessRule(new RegistryAccessRule(user,
                            RegistryRights.ReadKey | RegistryRights.Delete | RegistryRights.WriteKey | RegistryRights.ChangePermissions,
                            InheritanceFlags.None,
                            PropagationFlags.None,
                            AccessControlType.Allow));

                        try
                        {
                            using var key = root.OpenSubKey(currentKey, writable: true);
                            key.SetAccessControl(rs);
                        }
                        catch (Exception)
                        {
                            //ngbt.common.Tools.DumpException(e, $"Failed to set access permissions on {currentKey}");
                            thisKeyMustBeYielded = true;
                        }
                        if(!thisKeyMustBeYielded)
                        {
                            try
                            {
                                root.DeleteSubKey(currentKey);
                            }
                            catch (Exception)
                            {
                                //ngbt.common.Tools.DumpException(e, $"Failed to remove {currentKey} even with modified permissions");
                                thisKeyMustBeYielded = true;
                            }
                        }
                        if(thisKeyMustBeYielded)
                        {
                            yield return currentKey;
                        }
                    }
                }
            }
        }

        public static IEnumerable<string> FindString(this RegistryKey root, string keyPath, string valueToFind, bool recursive = true)
        {
            var subkeys = new Queue<string>();
            subkeys.Enqueue(keyPath);

            while (subkeys.Count > 0)
            {
                string currentKey = subkeys.Dequeue();
                using var key = root.OpenSubKey(currentKey, writable: false);
                foreach (var name in key.GetValueNames())
                {
                    if (key.GetValue(name) is string currentValue)
                    {
                        if (currentValue.Contains(valueToFind, StringComparison.OrdinalIgnoreCase))
                        {
                            yield return $"{currentKey}\\{name}";
                            break;
                        }
                    }
                }
                if (recursive)
                {
                    foreach (var name in key.GetSubKeyNames())
                    {
                        subkeys.Enqueue($"{currentKey}\\{name}");
                    }
                }
            }
        }
    }
}
