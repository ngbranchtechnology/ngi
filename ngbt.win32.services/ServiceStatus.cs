﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Runtime.InteropServices;
using Serilog;
using ngbt.common;

namespace ngbt.win32.services
{
    public class ServiceStatus : IDisposable
    {
        private readonly ILogger Log;
        private readonly Service Service;
        internal SERVICE_STATUS_PROCESS Status = new SERVICE_STATUS_PROCESS();
        private IntPtr Memory;

        public ServiceStatus(Service service)
        {
            Service = service;
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            Memory = Marshal.AllocHGlobal(Marshal.SizeOf(Status));
        }

        public bool Start()
        {
            if( !Service.IsValid )
            {
                Log.Warning("Warning, don't attempt to call StartService on {0}", Service);
                return false;
            }
            Log.Information("StartService({0})", Service);
            if (NativeServiceAPI.StartService(Service.Handle, 0, Memory))
                return true;

            return Log.WindowsError($"StartService({Service})");
        }

        public bool Control(SC_CONTROL_CODE code)
        {
            if (!Service.IsValid)
            {
                Log.Warning("Warning, don't attempt to call Control({1}) on {0}", Service, code);
                return false;
            }
            Log.Information("ControlService({0}, {1})", Service, code);
            if (NativeServiceAPI.ControlService(Service.Handle, code, Memory))
            {
                Status = (SERVICE_STATUS_PROCESS)Marshal.PtrToStructure(
                    Memory,
                    typeof(SERVICE_STATUS_PROCESS));
                Log.Information("Currentstatus = {0}", Status.CurrentState);
                return true;
            }
            return Log.WindowsError($"ControlService({Service}, {code})");
        }

        public bool Refresh()
        {
            if (!Service.IsValid)
            {
                Log.Warning("Warning, don't attempt to call QueryServiceStatusEx() on {0}", Service);
                return false;
            }
            if (NativeServiceAPI.QueryServiceStatusEx(
                Service.Handle,
                SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
                Memory,
                Marshal.SizeOf(Status),
                out int _))
            {
                Status = (SERVICE_STATUS_PROCESS)Marshal.PtrToStructure(
                    Memory,
                    typeof(SERVICE_STATUS_PROCESS));
                Log.Information("CurrentStatus as returned by QSSE = {0}", Status.CurrentState);
                return true;
            }
            return Log.WindowsError($"QueryServiceStatusEx({Service})");
        }

        #region IDisposable Members

        protected virtual void Dispose(bool disposing)
        {
            if (Memory.ToInt64() != 0)
            {
                if (disposing)
                {
                    Marshal.FreeHGlobal(Memory);
                    Memory = new IntPtr(0);
                }
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
