﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Runtime.InteropServices;
using Serilog;
using ngbt.common;

namespace ngbt.win32.services
{
    /// <summary>
    /// This is a wrapper class that provides object-oriented access to a Windows service. 
    /// The object is disposable, so that the underlying handle can be safely closed.
    /// </summary>
    public class Service : IDisposable
    {
        private readonly ILogger Log;
        public readonly string ServiceName;
        internal IntPtr Handle;

        public bool IsValid
        {
            get
            {
                if(Tools.Is64BitProcess)
                {
                    return Handle.ToInt64() != 0;
                }
                else
                {
                    return Handle.ToInt32() != 0;
                }
            }
        }


        /// <summary>
        /// The constructor opens access to the service
        /// </summary>
        /// <param name="scm">The SCM instance that contains the service</param>
        /// <param name="ServiceName">Name of the service</param>
        /// <param name="am">Access rights required.</param>
        public Service(
            ServiceControlManager scm,
            string serviceName,
            ACCESS_MASK am = ACCESS_MASK.STANDARD_RIGHTS_READ | ACCESS_MASK.GENERIC_READ)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            ServiceName = serviceName;

            Handle = NativeServiceAPI.OpenService(
                scm.Handle,
                serviceName,
                (uint)am);

            if (!IsValid)
            {
                Log.WindowsError($"OpenService({serviceName}, {am}) failed");
            }
        }

        /// <summary>
        /// Returns a copy of the current service configuration
        /// </summary>
        public QUERY_SERVICE_CONFIG ServiceConfig
        {
            get
            {
                if (!IsValid)
                {
                    Log.Warning("ServiceConfig not available for '{0}'", ServiceName);
                }
                else
                {
                    const int cbBufSize = 8 * 1024;
                    int cbBytesNeeded = 0;

                    IntPtr lpMemory = Marshal.AllocHGlobal((int)cbBufSize);

                    if (NativeServiceAPI.QueryServiceConfig(Handle, lpMemory, cbBufSize, ref cbBytesNeeded))
                    {
                        return (QUERY_SERVICE_CONFIG)
                            Marshal.PtrToStructure(
                                new IntPtr(lpMemory.ToInt32()),
                                typeof(QUERY_SERVICE_CONFIG));
                    }
                    Log.WindowsError($"QueryServiceConfig({ServiceName})");
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the text of the current service description
        /// </summary>
        public string Description
        {
            get
            {
                if (!IsValid)
                {
                    Log.Warning("Description not available for '{0}'", ServiceName);
                }
                else
                {
                    const int cbBufSize = 8 * 1024;
                    IntPtr lpMemory = Marshal.AllocHGlobal((int)cbBufSize);

                    if (NativeServiceAPI.QueryServiceConfig2(
                            Handle,
                            SC_SERVICE_CONFIG_INFO_LEVEL.SERVICE_CONFIG_DESCRIPTION,
                            lpMemory,
                            cbBufSize,
                            out int _))
                    {
                        var sd = (SERVICE_DESCRIPTION)
                            Marshal.PtrToStructure(
                                new IntPtr(lpMemory.ToInt32()),
                                typeof(SERVICE_DESCRIPTION));
                        return sd.Description;
                    }
                    Log.WindowsError($"QueryServiceConfig2({ServiceName})");
                }
                return null;
            }
            set
            {
                try
                {
                    var sd = new SERVICE_DESCRIPTION()
                    {
                        Description = value
                    };

                    const int cbBufSize = 8 * 1024;

                    IntPtr lpMemory = Marshal.AllocHGlobal((int)cbBufSize);
                    Marshal.StructureToPtr(sd, lpMemory, false);

                    bool result = NativeServiceAPI.ChangeServiceConfig2(
                        Handle,
                        SC_SERVICE_CONFIG.SERVICE_CONFIG_DESCRIPTION,
                        lpMemory);

                    Marshal.FreeHGlobal(lpMemory);
                }
                catch (Exception e)
                {
                    Log.Fatal(e, "ChangeServiceDescription() failed");
                }
            }
        }


        #region IDisposable Members

        protected virtual void Dispose(bool disposing)
        {
            if (Handle.ToInt64() != 0)
            {
                if (!NativeServiceAPI.CloseServiceHandle(Handle))
                {
                    Log.WindowsError("CloseServiceHandle() failed");
                }
                Handle = new IntPtr(0);
            }
        }
        ~Service()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
