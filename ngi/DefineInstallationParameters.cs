﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ngi.implementation;

namespace ngi
{
    public partial class DefineInstallationParameters : Form
    {
        private readonly Installation CurrentInstallation;

        public DefineInstallationParameters(Installation installation)
        {
            InitializeComponent();

            CurrentInstallation = installation;
            tbDescription.Text = installation.Description;
            tbFilename.Text = installation.Filename;
            tbName.Text = installation.Name;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            CurrentInstallation.Description = tbDescription.Text;
            CurrentInstallation.Filename = tbFilename.Text;
            CurrentInstallation.Name = tbName.Text;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
