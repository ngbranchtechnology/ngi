﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ngi.implementation;
using System.IO;
using Serilog;
using ngbt.common;

namespace ngi
{
    public partial class MainForm : Form
    {
        private readonly Runtime Runtime;
        private Installation ExistingInstallation;
        private readonly ILogger Log;
        private readonly Dictionary<string, ToolStripMenuItem> LookupMenuItems = new();

        public MainForm()
        {
            InitializeComponent();

            Runtime = new Runtime(new UIEventSink(this));
            Log = Tools.StaticLogger.ForContext("Context", ToString());

            MainMenuStrip = CreateMenuBar(
                CreateMenu("&Action",
                    CreateMenuItem("&Startup", "startup", onClick: StartupThisInstallation),
                    CreateMenuItem("&Shutdown", "shutdown", onClick: ShutdownThisInstallation),
                    CreateMenuItem("-"),
                    CreateMenuItem("&Uninstall", "uninstall", onClick: UninstallThisInstallation),
                    CreateMenuItem("-"),
                    CreateMenuItem("&Backup...", "backup", onClick: BackupThisInstallation),
                    CreateMenuItem("&Backup as...", "backup-as", onClick: BackupAsThisNewInstallation),
                    CreateMenuItem("-"),
                    CreateMenuItem("&Refresh", onClick: RefreshCurrentInstallation),
                    CreateMenuItem("-"),
                    CreateMenuItem("&Exit", onClick: CloseThisDialog)));

            Text = $"{Text} - Version {Tools.Version}";

            Controls.Add(MainMenuStrip);
            MainMenuStrip.PerformLayout();
        }

        private void CloseThisDialog(object sender, EventArgs e)
        {
            Close();
        }

        private void RefreshCurrentInstallation(object sender, EventArgs e)
        {
            ExistingInstallation = Installation.Detect(Log, false);
            bool bEnableMenu = true;
            if (ExistingInstallation == null)
                ExistingInstallation = Installation.Detect(Log, true);
            if (ExistingInstallation != null)
            {
                Log.Information("Found existing installation:\r\n- {0}", ExistingInstallation);
            }
            else
            {
                Log.Information("Did not find an existing installation.");
                bEnableMenu = false;
            }

            // hack: right now, we register only menuitems that need disablin under these circumstances
            foreach (var lmi in LookupMenuItems)
            {
                lmi.Value.Enabled = bEnableMenu;
            }
        }
        private void BackupThisInstallation(object sender, EventArgs e)
        {
            if (ExistingInstallation != null)
            {
                Runtime.Backup(ExistingInstallation, ExistingInstallation.Filename);
            }
        }

        private void BackupAsThisNewInstallation(object sender, EventArgs e)
        {
            if (ExistingInstallation != null)
            {
                // hack: second-guess the algorithm in Runtime():
                if (string.IsNullOrEmpty(ExistingInstallation.Filename))
                {
                    ExistingInstallation.Filename = $"{ExistingInstallation.Name} - {ExistingInstallation.Description}";
                }
                var dialog = new DefineInstallationParameters(ExistingInstallation);
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Runtime.Backup(ExistingInstallation, ExistingInstallation.Filename);
                }
            }
        }

        private void UninstallThisInstallation(object sender, EventArgs e)
        {
            if (ExistingInstallation != null)
            {
                Runtime.Uninstall(ExistingInstallation);
                RefreshCurrentInstallation(null, null);
            }
        }

        private void ShutdownThisInstallation(object sender, EventArgs e)
        {
            if (ExistingInstallation != null)
            {
                ExistingInstallation.Shutdown();
            }
        }

        private void StartupThisInstallation(object sender, EventArgs e)
        {
            if (ExistingInstallation != null)
            {
                ExistingInstallation.Startup();
            }
        }

        private void InstallThis(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem tsmi)
            {
                if (tsmi.Tag is Installation newInstallation)
                {
                    Runtime.SwitchToThisOneInstallation(ExistingInstallation, newInstallation);
                    RefreshCurrentInstallation(null, null);
                }
            }
        }


        private static MenuStrip CreateMenuBar(params ToolStripMenuItem[] menus)
        {
            var result = new MenuStrip();
            result.Items.AddRange(menus);
            return result;
        }

        private static ToolStripMenuItem CreateMenu(string caption, params ToolStripItem[] menus)
        {
            var result = new ToolStripMenuItem();
            result.DropDownItems.AddRange(menus);
            result.Text = caption;
            return result;
        }

        private ToolStripItem CreateMenuItem(string caption, string stringId = null, EventHandler onClick = null)
        {
            if (caption == "-")
            {
                return new ToolStripSeparator();
            }
            var result = new ToolStripMenuItem() { Text = caption };
            if (onClick != null)
            {
                result.Click += onClick;
            }
            if (!string.IsNullOrEmpty(stringId))
            {
                LookupMenuItems[stringId] = result;
            }
            return result;
        }

        public List<ListViewItem> Write(string output)
        {
            var result = new List<ListViewItem>();
            foreach (string line in output.Split('\n'))
            {
                result.Add(listView1.Items.Add(line));
            }
            return result;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Log.Information("*** NGBT Installation Manager - Version {0} ***\r\nCopyright (C) 2020 by NG Branch Technology GmbH", version);

            RefreshCurrentInstallation(null, null);
            int nInstallations = 0;

            var proaktMenuStrip = new List<ToolStripItem>();
            var ng1MenuStrip = new List<ToolStripItem>();
            var arcaMenuStrip = new List<ToolStripItem>();
            var ngclassicMenuStrip = new List<ToolStripItem>();
            var installationsMenuStrip = new List<ToolStripItem>();

            foreach (var installation in Runtime.GetInstallations())
            {
                ++nInstallations;
                List<ToolStripItem> installToThisStrip = null;

                if (installation.Name.Contains("proakt", StringComparison.OrdinalIgnoreCase))
                {
                    installToThisStrip = proaktMenuStrip;
                }
                else if (installation.Name.Contains("arca", StringComparison.OrdinalIgnoreCase))
                {
                    installToThisStrip = arcaMenuStrip;
                }
                else if (installation.Name.Contains("ng1", StringComparison.OrdinalIgnoreCase))
                {
                    installToThisStrip = ng1MenuStrip;
                }
                else if (installation.Name.Contains("NG", StringComparison.OrdinalIgnoreCase))
                {
                    installToThisStrip = ngclassicMenuStrip;
                }
                else
                {
                    installToThisStrip = installationsMenuStrip;
                }

                var filename = Path.GetFileName(installation.Filename);
                var mi = CreateMenuItem($"{installation.Name} - {installation.Description}", onClick: InstallThis);
                mi.Tag = installation;
                installToThisStrip.Add(mi);
            }

            AddSortedMenuItems(proaktMenuStrip, "ProAKT");
            AddSortedMenuItems(ngclassicMenuStrip, "NG Classic");
            AddSortedMenuItems(arcaMenuStrip, "ARCA");
            AddSortedMenuItems(ng1MenuStrip, "NG1");
            AddSortedMenuItems(installationsMenuStrip, "Other");
            MainMenuStrip.PerformLayout();

            if (nInstallations == 0)
            {
                Log.Warning("- Sorry, did not find any backup installations");
            }
            else
            {
                Log.Warning("- Found a total of ({0}) installation(s)", nInstallations);
            }
        }

        private int CompareInstallations(ToolStripItem x, ToolStripItem y)
        {
            return x.Text.CompareTo(y.Text);
        }

        private void AddSortedMenuItems(List<ToolStripItem> menuItems, string caption)
        {
            if (menuItems.Count > 0)
            {
                var menuStrip = new ToolStripMenuItem() { Text = caption };
                menuItems.Sort(CompareInstallations);
                foreach (var mi in menuItems)
                {
                    menuStrip.DropDownItems.Add(mi);
                }
                MainMenuStrip.Items.Add(menuStrip);
            }
        }
    }
}
