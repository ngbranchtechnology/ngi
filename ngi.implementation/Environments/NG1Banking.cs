﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System.Runtime.Versioning;
using ngi.implementation.Tasks;
using ngi.implementation.Instructions;
using System.IO;
using System.Diagnostics;

namespace ngi.implementation.Environments
{
    [SupportedOSPlatform("windows")]
    public static class NG1Banking 
    {

        public static Installation Detect(string abstractInstallDir, string physicalInstallDir)
        {
            string strServerFilename = Path.Combine(physicalInstallDir, "server\\ng1_server.exe");
            var myFileVersionInfo = FileVersionInfo.GetVersionInfo(strServerFilename);
            string version = $"{myFileVersionInfo.FileMajorPart}.{myFileVersionInfo.FileMinorPart}.{myFileVersionInfo.FileBuildPart}.{myFileVersionInfo.FilePrivatePart}";
            
            var result = new Installation(
                $"NG1 Banking {version}",
                "Release-Date Unknown");

            string abstractAppDataDir = "%ProgramData%\\NG1 Banking";

            result.ReplacementVariables["$$INSTDIR$$"] = abstractInstallDir;
            result.ReplacementVariables["$$APPDATA$$"] = abstractAppDataDir;

            result.ShutdownItems.Add(new ExecuteProcessTask(
                Path.Combine("$$INSTDIR$$\\server", "ng1_startup.exe"),
                "/shutdown"));

            // this list could be optimized, but for now we just do it all:
            result.ShutdownItems.Add(new KillProcessTask("ng1_watchdog"));
            result.ShutdownItems.Add(new KillProcessTask("ng1_server"));
            result.ShutdownItems.Add(new KillProcessTask("ng1_startup"));
            result.ShutdownItems.Add(new KillProcessTask("ng1_tools"));

            // this list could be optimized, but for now we just do it all:
            result.StartupItems.Add(new ExecuteProcessTask(
                Path.Combine("$$INSTDIR$$\\server", "ng1_startup.exe")));

            result.Instructions.Add(new CopyFilesInstruction(
                    abstractAppDataDir,
                    "Data",
                    recursive: true,
                    excludePattern: "*.LOG*"));

            result.Instructions.Add(new CopyFilesInstruction(
                abstractInstallDir,
                "Files",
                recursive: true));

            if (InstallerInstruction.Detect("NG1 Banking", out var ii))
            {
                result.Instructions.Add(ii);
            }

            return result;
        }
    }
}
