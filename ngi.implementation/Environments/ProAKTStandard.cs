﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using Microsoft.Win32;
using System.Runtime.Versioning;
using ngi.implementation.Tasks;
using ngi.implementation.Instructions;
using System.IO;
using System.Diagnostics;
using ngbt.common;
using ngbt.win32.registry;
using ngbt.win32.services;
using System.Collections.Generic;

namespace ngi.implementation.Environments
{
    [SupportedOSPlatform("windows")]
    public static class ProAKTStandard 
    {

        public static Installation Detect(RegistryKey key)
        {
            string version = key.GetStringValue("ProaktVersion");
            string releaseDate = key.GetStringValue("ReleaseDate");

            string physicalInstallDir = key.GetStringValue("Install_Dir");
            if (!physicalInstallDir.EndsWith("\\"))
                physicalInstallDir += "\\";

            string abstractInstallDir = Tools.ShortenName(physicalInstallDir);

            string physicalAppDataDir = key.GetStringValue("AppDataDir");

            if (!physicalAppDataDir.EndsWith("\\"))
                physicalAppDataDir += "\\";

            string abstractAppDataDir = Tools.ShortenName(physicalAppDataDir);

            // older ARCA Branch version => need to retrieve version number "manually"
            if (string.IsNullOrEmpty(version))
            {
                var myFileVersionInfo = FileVersionInfo.GetVersionInfo(Path.Combine(physicalInstallDir, "STARTAKT.EXE"));
                releaseDate = myFileVersionInfo.SpecialBuild;
                version = $"{myFileVersionInfo.FileMajorPart}.{myFileVersionInfo.FileMinorPart}.{myFileVersionInfo.FileBuildPart}.{myFileVersionInfo.FilePrivatePart}";
            }

            var result = new Installation(
                $"ProAKT Standard {version}",
                $"Released {releaseDate}");

            result.ReplacementVariables["$$INSTDIR$$"] = physicalInstallDir[..^1];
            result.ReplacementVariables["$$APPDATA$$"] = physicalAppDataDir[..^1];

            result.ShutdownItems.Add(new ExecuteProcessTask(
                Path.Combine("$$INSTDIR$$", "STOPAKT.EXE"),
                "/FORCE"));

            var serviceNames = new List<string>();
            using(var scm = new ServiceControlManager())
            {
                var services = scm.GetServiceNames();
                if(services.Contains("PASERVER"))
                {
                    serviceNames.Add("PASERVER");
                }
                if (services.Contains("JBOS_BACKGROUND_SERVICE"))
                {
                    serviceNames.Add("JBOS_BACKGROUND_SERVICE");
                }
            }

            foreach(var serviceName in serviceNames)
            {
                result.ShutdownItems.Add(new ShutdownService(serviceName));
            }

            // this list could be optimized, but for now we just do it all:
            result.ShutdownItems.Add(new KillProcessTask("PACLIENT"));
            result.ShutdownItems.Add(new KillProcessTask("PROAKT_TOUCH"));
            result.ShutdownItems.Add(new KillProcessTask("PASERVER"));
            result.ShutdownItems.Add(new KillProcessTask("JBOS_BACKGROUND_SERVICE"));

            // this list could be optimized, but for now we just do it all:
            result.StartupItems.Add(new ExecuteProcessTask(
                Path.Combine("$$INSTDIR$$", "STARTAKT.EXE")));

            foreach (var serviceName in serviceNames)
            {
                result.Instructions.Add(new ServiceInstruction(
                    serviceName,
                    $"Services\\{serviceName}.txt"));
            }

            result.Instructions.Add(new CopyFilesInstruction(
                abstractAppDataDir,
                "Data",
                recursive: true,
                excludePattern: "*.LOG*"));

            result.Instructions.Add(new CopyFilesInstruction(
                abstractInstallDir,
                "Files",
                recursive: true));

            result.Instructions.Add(new RegistryInstruction(
                "HKEY_CLASSES_ROOT\\WOSA/XFS_ROOT",
                "Registry\\wosaxfs.txt"));

            result.Instructions.Add(new RegistryInstruction(
                "HKEY_LOCAL_MACHINE\\Software\\Pergamon",
                "Registry\\pergamon.txt"));

            if(InstallerInstruction.Detect("ProAKT", out var ii))
            {
                result.Instructions.Add(ii);
            }
            return result;
        }
    }
}
