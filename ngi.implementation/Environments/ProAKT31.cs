﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Runtime.Versioning;
using ngi.implementation.Tasks;
using ngi.implementation.Instructions;
using System.IO;
using System.Diagnostics;
using ngbt.common;
using ngbt.win32.registry;
using ngbt.win32.services;
using Serilog;

namespace ngi.implementation.Environments
{
    [SupportedOSPlatform("windows")]
    public static class ProAKT31 
    {

        public static Installation Detect(ILogger log, RegistryKey key, string physicalInstallDir)
        {
            string abstractInstallDir = Tools.ShortenName(physicalInstallDir);

            string strcombinedPath = Path.Combine(physicalInstallDir, "STARTAKT.EXE");
            Console.WriteLine(strcombinedPath);
            var myFileVersionInfo = FileVersionInfo.GetVersionInfo(strcombinedPath);
            var date = File.GetLastWriteTime(strcombinedPath).Date;
            string releaseDate = $"{date.Day}.{date.Month}.{date.Year}";
            string version = $"{myFileVersionInfo.FileMajorPart}.{myFileVersionInfo.FileMinorPart}.{myFileVersionInfo.FileBuildPart}.{myFileVersionInfo.FilePrivatePart}";

            string strApplicationTitle = $"ProAKT {version}";
            using (var subkey = key.OpenSubKey("Anwendung\\Options", false))
            {
                string strConfiguredTitle = subkey.GetStringValue("ApplicationTitle");
                if (!string.IsNullOrEmpty(strConfiguredTitle))
                {
                    strApplicationTitle = strConfiguredTitle.Replace("$$VERSION$$", version);
                    Console.WriteLine($"strApplicationTitle as {strApplicationTitle}");
                }
            }

            var result = new Installation(
                strApplicationTitle,
                $"Released {releaseDate}");

            result.ReplacementVariables["$$INSTDIR$$"] = physicalInstallDir[..^1];

            result.ShutdownItems.Add(new ExecuteProcessTask(
                Path.Combine("$$INSTDIR$$", "STOPAKT.EXE"),
                "/FORCE"));

            var serviceNames = new List<string>();
            using (var scm = new ServiceControlManager())
            {
                var services = scm.GetServiceNames();
                if (services.Contains("PASERVER"))
                {
                    serviceNames.Add("PASERVER");
                }
                if (services.Contains("JBOS_BACKGROUND_SERVICE"))
                {
                    serviceNames.Add("JBOS_BACKGROUND_SERVICE");
                }
                if (services.Contains("OPOSWRAPPERSERVER"))
                {
                    serviceNames.Add("OPOSWRAPPERSERVER");
                }
                if (services.Contains("ISLANDCLIENT"))
                {
                    serviceNames.Add("ISLANDCLIENT");
                }
            }

            foreach (var serviceName in serviceNames)
            {
                result.ShutdownItems.Add(new ShutdownService(serviceName));
            }

            // this list could be optimized, but for now we just do it all:
            result.ShutdownItems.Add(new KillProcessTask("PACLIENT"));
            result.ShutdownItems.Add(new KillProcessTask("PASERVER"));

            // this list could be optimized, but for now we just do it all:
            result.StartupItems.Add(new ExecuteProcessTask(
                Path.Combine("$$INSTDIR$$", "STARTAKT.EXE")));

            foreach (var serviceName in serviceNames)
            {
                result.Instructions.Add(new ServiceInstruction(
                    serviceName,
                    $"Services\\{serviceName}.txt"));
            }

            result.Instructions.Add(new CopyFilesInstruction(
                abstractInstallDir,
                "Files",
                recursive: true));

            result.Instructions.Add(new RegistryInstruction(
                "HKEY_CLASSES_ROOT\\WOSA/XFS_ROOT",
                "Registry\\wosaxfs.txt"));

            result.Instructions.Add(new RegistryInstruction(
                "HKEY_LOCAL_MACHINE\\Software\\Pergamon",
                "Registry\\pergamon.txt"));

            if (InstallerInstruction.Detect("ProAKT", out var ii))
            {
                result.Instructions.Add(ii);
            }
            return result;
        }
    }
}
