﻿using System;
using Serilog;
using System.IO;
using System.IO.Compression;
using ngbt.common;

namespace ngi.implementation.Archives
{
    public class ZipArchiveWriter : IDisposable, IArchiveWriter
    {
        private readonly ILogger Log;
        private FileStream ZipFile;
        private ZipArchive Archive;
        private bool DisposedValue;
        private readonly CompressionLevel DefaultCompressionLevel;

        public ZipArchiveWriter(string filename)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            ZipFile = new FileStream(filename, FileMode.Create);
            Archive = new ZipArchive(ZipFile, ZipArchiveMode.Create);
            DefaultCompressionLevel = CompressionLevel.Fastest;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    if(Archive != null)
                    {
                        Archive.Dispose();
                        Archive = null;
                    }

                    if (ZipFile != null)
                    {
                        ZipFile.Dispose();
                        ZipFile = null;
                    }
                }

                DisposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public bool WriteLocalFile(string localName, string archiveName)
        {
            try
            {
                // TODO: make compression level confiurable
                Archive.CreateEntryFromFile(localName, archiveName, DefaultCompressionLevel);
                return true;
            }
            catch(Exception e)
            {
                Log.Fatal(e, "Failed to write {0} => {1}", localName, archiveName);
                return false;
            }
        }

        public bool WriteString(string stringContent, string archiveName)
        {
            try
            {
                // TODO: make compression level confiurable
                var zae = Archive.CreateEntry(archiveName, DefaultCompressionLevel);
                using (StreamWriter writer = new StreamWriter(zae.Open(), encoding: System.Text.Encoding.UTF8))
                {
                    writer.Write(stringContent);
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Failed to create {0}", archiveName);
                return false;
            }
        }
    }
}
