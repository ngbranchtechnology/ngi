﻿using System;
using Serilog;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using ngbt.common;

namespace ngi.implementation.Archives
{
    public class ZipArchiveReader : IDisposable, IArchiveReader
    {
        private readonly ILogger Log;
        private FileStream ZipFile;
        private ZipArchive Archive;
        private bool DisposedValue;

        public ZipArchiveReader(string filename)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            ZipFile = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            Archive = new ZipArchive(ZipFile, ZipArchiveMode.Read);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    if(Archive != null)
                    {
                        Archive.Dispose();
                        Archive = null;
                    }

                    if (ZipFile != null)
                    {
                        ZipFile.Dispose();
                        ZipFile = null;
                    }
                }

                DisposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public bool ReadLocalFile(string archiveName, string localName)
        {
            try
            {
                var ae = Archive.GetEntry(archiveName);
                if(ae == null)
                {
                    return false;
                }
                if(File.Exists(localName))
                {
                    File.SetAttributes(localName, FileAttributes.Normal);
                    File.Delete(localName);
                }
                ae.ExtractToFile(localName);
                return true;
            }
            catch(Exception e)
            {
                Log.Fatal(e, "Failed to read {0} <= {1}", localName, archiveName);
                return false;
            }
        }

        public string ReadString(string archiveName)
        {
            try
            {
                var ae = Archive.GetEntry(archiveName);
                if(ae == null)
                    ae = Archive.GetEntry(archiveName.Replace('\\', '/'));
                using var sr = new StreamReader(ae.Open());
                return sr.ReadToEnd();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Failed to read {0}", archiveName);
                return null;
            }
        }

        public IEnumerable<string> GetMatchingEntries(string pattern)
        {
            foreach(var item in Archive.Entries)
            {
                if(item.FullName.StartsWith(pattern, StringComparison.OrdinalIgnoreCase))
                {
                    yield return item.FullName;
                }
            }
        }

    }
}
