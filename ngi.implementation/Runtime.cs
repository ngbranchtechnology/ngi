﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using Microsoft.Extensions.Configuration;
using ngbt.common;
using Serilog;
using Serilog.Core;
using ngbt.win32.registry;

namespace ngi.implementation
{
    [SupportedOSPlatform("windows")]
    public class Runtime
    {
        private readonly ILogger Log;
        public readonly List<string> InstallationFolders = new List<string>();
        public bool Startup = false;
        public bool Shutdown = false;

        public Runtime(ILogEventSink eventSink = null)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("config\\logging.json")
                .Build();

            var lconf = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration);

            if (eventSink != null)
            {
                lconf.WriteTo.Sink(eventSink);
            }

            var logger = lconf.CreateLogger();

            Tools.StaticLogger = logger.ForContext("Context", "ngi.implementation");
            Log = logger.ForContext("Context", ToString());
            Regis3.Log = logger.ForContext("Context", "ngbt.win32.registry");

            IConfigurationRoot configRoot = new ConfigurationBuilder()
                .AddJsonFile($"config\\ngi.json", optional: true, reloadOnChange: true)
                .Build();

            foreach (var installationFolder in configRoot.GetSection("InstallationFolders").GetChildren())
            {
                var folder = installationFolder.Value;
                if (!Directory.Exists(folder))
                {
                    Log.Error("ERROR: configuration says NGI backups should be stored in {0}, but this folder is not accessible.", folder);
                    throw new DirectoryNotFoundException(folder);
                }
                InstallationFolders.Add(folder);
            }
        }

        public bool Backup(Installation installation, string filename)
        {
            if (installation == null)
            {
                Log.Information("ERROR, no installation detected => cannot backup");
                return false;
            }
            if (string.IsNullOrEmpty(filename))
            {
                if (!string.IsNullOrEmpty(installation.Filename))
                {
                    filename = installation.Filename;
                }
                else
                {
                    filename = $"{installation.Name} - {installation.Description}";
                }
            }
            if (string.IsNullOrEmpty(filename))
            {
                Log.Information("ERROR, you must specify at least a filename");
                return false;
            }
            if (!filename.Contains("\\"))
            {
                foreach (var installationFolder in InstallationFolders)
                {
                    filename = Path.Combine(installationFolder, filename);
                    break;
                }
            }
            if (!filename.EndsWith(".ngi", StringComparison.OrdinalIgnoreCase))
            {
                filename += ".ngi";
            }

            Log.Information("Creating backup {0}", filename);
            if (installation.Backup(filename))
            {
                Log.Information("Done.");
                return true;
            }
            Log.Information("Warning: unable to backup installation");
            return false;
        }

        public bool Uninstall(Installation installation)
        {
            if (installation != null)
            {
                Log.Information("Removing {0}", installation);
                if (installation.Uninstall())
                {
                    Log.Information("Done.");
                    return true;
                }
                Log.Information("Warning: unable to completely remove existing installation: please check manually");
                return false;
            }
            else
            {
                Log.Information("Warning: no installation found: please check manually");
                return false;
            }
        }

        public bool Restore(Installation existingInstallation, string restorename)
        {
            if(string.IsNullOrEmpty(restorename))
            {
                Log.Information("Error: bad parameters for /RESTORE <name>");
                return false;
            }

            var candidates = new List<Installation>();
            foreach (var possibleInstallation in GetInstallations())
            {
                if (possibleInstallation.Filename.Contains(restorename, StringComparison.OrdinalIgnoreCase) ||
                    possibleInstallation.Name.Contains(restorename, StringComparison.OrdinalIgnoreCase) ||
                    possibleInstallation.Description.Contains(restorename, StringComparison.OrdinalIgnoreCase))
                {
                    candidates.Add(possibleInstallation);
                }
            }
            if (candidates.Count == 1)
            {
                return SwitchToThisOneInstallation(existingInstallation, candidates[0]);
            }
            else if (candidates.Count > 1)
            {
                Log.Information("Ambiguous specification. Possible matches are:");
                foreach (var installation in candidates)
                {
                    Log.Information("{0}:\r\n- {1}", installation.Filename, installation);
                }
            }
            else if (candidates.Count == 0)
            {
                Log.Information("No match found. Possible candidates are:");
                ListInstallations();
            }
            return false;
        }

        public bool SwitchToThisOneInstallation(Installation existingInstallation, Installation newInstallation)
        {
            if (existingInstallation != null)
            {
                Log.Information("Removing this: {0}", existingInstallation);
                existingInstallation.Uninstall();
            }
            Log.Information("Restoring this: {0}", newInstallation);
            if (!newInstallation.Restore())
                return false;

            if (Startup)
            {
                return newInstallation.Startup();
            }
            return true;
        }

        public IEnumerable<Installation> GetInstallations()
        {
            foreach (string folder in InstallationFolders)
            {
                var eopts = new EnumerationOptions() { RecurseSubdirectories = true };
                foreach (string file in Directory.EnumerateFiles(folder, "*.ngi", eopts))
                {
                    var installation = Installation.FromArchiveInstructions(Log, file);
                    if (installation != null)
                    {
                        yield return installation;
                    }
                }
            }
        }

        public bool ListInstallations()
        {
            bool foundAnything = false;
            foreach (var installation in GetInstallations())
            {
                Log.Information("{0}:\r\n- {1}", installation.Filename, installation);
                foundAnything = true;
            }
            if (!foundAnything)
            {
                Log.Warning("- Sorry, did not find any backup installations");
            }
            return false;
        }
    }
}
