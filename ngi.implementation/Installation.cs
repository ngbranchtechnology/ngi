﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Runtime.Versioning;
using ngi.implementation.Environments;
using System.Xml;
using Serilog;
using System.Text;
using System.IO;
using ngi.implementation.Instructions;
using ngi.implementation.Tasks;
using ngbt.common;

namespace ngi.implementation
{
    [SupportedOSPlatform("windows")]
    public class Installation
    {
        private readonly ILogger Log;
        public static string INSTALLATION_XML = "installation.xml";
        public string Name;
        public string Description;
        public string Filename;
        public readonly List<IInstruction> Instructions = new List<IInstruction>();
        public readonly List<ITask> StartupItems = new List<ITask>();
        public readonly List<ITask> ShutdownItems = new List<ITask>();
        public readonly Dictionary<string, string> ReplacementVariables = new Dictionary<string, string>()
        {
            { "$$MACHINE$$", System.Environment.MachineName }
        };

        public Installation(string name, string description)
        {
            Name = name;
            Description = description;
            Log = Tools.StaticLogger.ForContext("Context", ToString());
        }

        public override string ToString()
        {
            return $"{Name} [{Description}]";
        }

        public bool Restore(string filename = null)
        {
            if (filename == null)
                filename = Filename;
            Log.Information("BEGIN Restore {0}", Name);
            using (var archive = new Archives.ZipArchiveReader(filename))
            {
                foreach (var instruction in Instructions)
                {
                    if (!instruction.Restore(archive, ReplacementVariables))
                    {
                        Log.Warning("END Restore {0} with failure", Name);
                        return false;
                    }
                }

                if (ReplacementVariables.TryGetValue("$$INSTDIR$$", out string installDir))
                {
                    File.WriteAllText(
                        Path.Combine(Tools.ExpandName(installDir), INSTALLATION_XML),
                        CreateInstallationXml(),
                        Encoding.UTF8);
                }
            }
            Log.Information("END Restore {0} successfully", Name);
            return true;
        }

        public bool Uninstall()
        {
            Log.Information("BEGIN Uninstall {0}", Name);
            Shutdown();
            foreach (var instruction in Instructions)
            {
                if (!instruction.Uninstall(ReplacementVariables))
                {
                    Log.Warning("END Uninstall {0} with failure", Name);
                    return false;
                }
            }
            Log.Information("END Uninstall {0} successfully", Name);
            return true;
        }

        public bool Shutdown()
        {
            Log.Information("BEGIN Shutdown {0}", Name);
            foreach (var instruction in ShutdownItems)
            {
                try
                {
                    if (!instruction.Run(ReplacementVariables))
                    {
                        Log.Warning("END Shutdown {0} with failure on {1}", Name, instruction);
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Log.Fatal(e, "END Shutdown {0} with exception in {1}", Name, instruction);
                    return false;
                }
            }
            Log.Information("END Shutdown {0} successfully", Name);
            return true;
        }

        public bool Startup()
        {
            Log.Information("BEGIN Startup {0}", Name);
            foreach (var instruction in StartupItems)
            {
                if (!instruction.Run(ReplacementVariables))
                {
                    Log.Warning("END Startup {0} with failure", Name);
                    return false;
                }
            }
            Log.Information("END Startup {0} successfully", Name);
            return true;
        }

        public string CreateInstallationXml()
        {
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Indent = true
            };
            StringBuilder builder = new StringBuilder();

            using (XmlWriter writer = XmlWriter.Create(builder, settings))
            {
                writer.WriteStartDocument();
                WriteInstallationXml(writer, ReplacementVariables);
                writer.WriteEndDocument();
            }
            return builder.ToString();
        }

        public void WriteInstallationXml(XmlWriter writer, Dictionary<string, string> lookup)
        {
            writer.WriteStartElement("Installation");
            writer.WriteAttributeString("Name", Name);
            writer.WriteAttributeString("Description", Description);

            writer.WriteStartElement("ReplacementVariables");
            foreach (var key in lookup.Keys)
            {
                writer.WriteStartElement("Replace");
                writer.WriteAttributeString("Key", key);
                if (key.Equals("$$MACHINE$$"))
                {
                    writer.WriteAttributeString("Value", "%ComputerName%");
                }
                else
                {
                    writer.WriteAttributeString("Value", Tools.ShortenName(lookup[key]));
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement(); // StartupItems

            writer.WriteStartElement("StartupItems");
            foreach (var instruction in StartupItems)
            {
                instruction.WriteInstallationXml(writer, lookup);
            }
            writer.WriteEndElement(); // StartupItems

            writer.WriteStartElement("Instructions");
            foreach (var instruction in Instructions)
            {
                instruction.WriteInstallationXml(writer, lookup);
            }
            writer.WriteEndElement(); // Instructions

            writer.WriteStartElement("ShutdownItems");
            foreach (var instruction in ShutdownItems)
            {
                instruction.WriteInstallationXml(writer, lookup);
            }
            writer.WriteEndElement(); // ShutdownItems
            writer.WriteEndElement(); // Installation
        }

        public bool Backup(string filename)
        {
            Log.Information("BEGIN Backup {0} to {1}", Name, filename);
            Shutdown();
            using (var output = new Archives.ZipArchiveWriter(filename))
            {
                output.WriteString(CreateInstallationXml(), INSTALLATION_XML);

                foreach (var instruction in Instructions)
                {
                    if (!instruction.Backup(output, ReplacementVariables))
                    {
                        Log.Warning("END Backup {0} to {1} with failure", Name, filename);
                        return false;
                    }
                }
            }
            Log.Information("END Backup {0} to {1} successfully", Name, filename);
            return true;
        }
        enum ReadMode
        {
            Default,
            ReadStartupItems,
            ReadInstructions,
            ReadShutdownItems
        };

        private delegate IInstruction InstructionCreator(XmlReader reader);
        private delegate ITask TaskCreator(XmlReader reader);

        public static Installation FromXmlInstructionsFile(ILogger log, string fileName)
        {
            log.Information("Reading instructions from file: '{0}'", fileName);
            return FromXmlInstructionsString(log, File.ReadAllText(fileName, Encoding.UTF8), fileName);
        }

        public static Installation FromArchiveInstructions(ILogger log, string archiveName)
        {
            using var input = new Archives.ZipArchiveReader(archiveName);
            string instructions = input.ReadString(INSTALLATION_XML);
            if (instructions == null)
            {
                log.Error("Did not find instructions.xml in {0}, assuming this is not a valid archive...", archiveName);
                return null;
            }
            return FromXmlInstructionsString(log, instructions, archiveName);
        }

        public static Installation FromXmlInstructionsString(ILogger log, string instructions, string filename)
        {
            // this code could be remade more nicely, but I don't have the willpower to do it
            var knownInstructions = new Dictionary<string, InstructionCreator>()
            {
                { "RegistryInstruction", RegistryInstruction.FromXml },
                { "ServiceInstruction", ServiceInstruction.FromXml },
                { "CopyFilesInstruction", CopyFilesInstruction.FromXml },
            };

            var knownTasks = new Dictionary<string, TaskCreator>()
            {
                { "ExecuteProcessTask", ExecuteProcessTask.FromXml },
                { "KillProcessTask", KillProcessTask.FromXml },
                { "ShutdownService", ShutdownService.FromXml },
            };

            Installation result = null;
            ReadMode rm = ReadMode.Default;
            using var sr = new StringReader(instructions);
            using var reader = XmlReader.Create(sr);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (rm == ReadMode.Default)
                        {
                            if (reader.Name.Equals("Installation", StringComparison.OrdinalIgnoreCase))
                            {
                                result = new Installation(
                                    reader.GetAttribute("Name"),
                                    reader.GetAttribute("Description")
                                )
                                { Filename = filename };
                            }
                            else if (reader.Name.Equals("StartupItems", StringComparison.OrdinalIgnoreCase))
                            {
                                rm = ReadMode.ReadStartupItems;
                            }
                            else if (reader.Name.Equals("ShutdownItems", StringComparison.OrdinalIgnoreCase))
                            {
                                rm = ReadMode.ReadShutdownItems;
                            }
                            else if (reader.Name.Equals("Instructions", StringComparison.OrdinalIgnoreCase))
                            {
                                rm = ReadMode.ReadInstructions;
                            }
                            else if (reader.Name.Equals("Replace", StringComparison.OrdinalIgnoreCase))
                            {
                                result.ReplacementVariables[reader.GetAttribute("Key")] = reader.GetAttribute("Value");
                            }
                        }
                        else if (rm == ReadMode.ReadInstructions)
                        {
                            if (knownInstructions.TryGetValue(reader.Name, out var instructionCreator))
                            {
                                result.Instructions.Add(instructionCreator(reader));
                            }
                        }
                        else if (rm == ReadMode.ReadStartupItems)
                        {
                            if (knownTasks.TryGetValue(reader.Name, out var taskCreator))
                            {
                                result.StartupItems.Add(taskCreator(reader));
                            }
                        }
                        else if (rm == ReadMode.ReadShutdownItems)
                        {
                            if (knownTasks.TryGetValue(reader.Name, out var taskCreator))
                            {
                                result.ShutdownItems.Add(taskCreator(reader));
                            }
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if (reader.Name.Equals("StartupItems", StringComparison.OrdinalIgnoreCase))
                        {
                            rm = ReadMode.Default;
                        }
                        else if (reader.Name.Equals("ShutdownItems", StringComparison.OrdinalIgnoreCase))
                        {
                            rm = ReadMode.Default;
                        }
                        else if (reader.Name.Equals("Instructions", StringComparison.OrdinalIgnoreCase))
                        {
                            rm = ReadMode.Default;
                        }
                        break;
                }
            }
            return result;
        }


        public static Installation Detect(ILogger log, bool bIs64 = false)
        {
            try
            {
                // check the registry to see if we can find the current environment
                using var rootKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, bIs64 ? RegistryView.Registry64 : RegistryView.Registry32);
                var key = rootKey.OpenSubKey("Software\\Pergamon\\AKT");
                if (key == null)
                    key = rootKey.OpenSubKey("Software\\NGBT\\PROAKT\\AKT");
                if (key != null)
                {

                    var value = key.GetValue("Install_Type");
                    if (value != null)
                    {
                        if (value is string installType)
                        {
                            if (installType.Equals("ARCA Branch", StringComparison.OrdinalIgnoreCase))
                            {
                                return ArcaBranch.Detect(key);
                            }
                            else if (installType.Equals("ProAKT Standard", StringComparison.OrdinalIgnoreCase))
                            {
                                return ProAKTStandard.Detect(key);
                            }
                            else if (installType.Equals("NG Classic", StringComparison.OrdinalIgnoreCase))
                            {
                                return NGClassic.Detect(key);
                            }
                        }
                    }
                    value = key.GetValue("Install_Dir");
                    if (value != null)
                    {
                        if (value is string strInstallFolder)
                        {
                            if (!strInstallFolder.EndsWith("\\"))
                                strInstallFolder += "\\";

                            string strInstallationXml = $"{strInstallFolder}{Installation.INSTALLATION_XML}";
                            if (File.Exists(strInstallationXml))
                            {
                                return FromXmlInstructionsFile(log, strInstallationXml);
                            }

                            return ProAKT31.Detect(log, key, strInstallFolder);
                        }
                    }
                }
                string strBankingFolder = "%ProgramFiles%\\NG1 Banking";
                string strExpandedBankingFolder = Tools.ExpandName(strBankingFolder);
                if (Directory.Exists(strExpandedBankingFolder))
                {
                    return NG1Banking.Detect(strBankingFolder, strExpandedBankingFolder);
                }
            }
            catch (Exception e)
            {
                log.Fatal(e, "Unable to determine installation type");
            }
            return null;
        }




    }
}
