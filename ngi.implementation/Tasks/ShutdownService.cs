﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System.Xml;
using System.Collections.Generic;
using ngbt.win32.services;

namespace ngi.implementation.Tasks
{

    /// This is a instruction that can be used to shutdown a service, before attempting a backup / uninstall / restore operation
    public class ShutdownService : ITask
    {
        private readonly string ServiceName;

        public ShutdownService(string serviceName)
        {
            ServiceName = serviceName;
        }

        ///-------------------------------------------------------------------------------------------------
        /// Writes an installation XML part
        ///
        /// \param  writer The writer.
        /// \param  lookup The lookup.

        public void WriteInstallationXml(XmlWriter writer, Dictionary<string, string> lookup)
        {
            writer.WriteStartElement("ShutdownService");
            writer.WriteAttributeString("Name", ServiceName);
            writer.WriteEndElement(); // ShutdownService
        }

        public static ITask FromXml(XmlReader reader)
        {
            return new ShutdownService(
                serviceName: reader.GetAttribute("Name")
            );
        }
        ///-------------------------------------------------------------------------------------------------
        /// The Run() function for this task shuts down the service.
        ///
        /// \param  lookup dictionary - not used in this class
        ///
        /// \returns True if it succeeds, false if it fails.

        public bool Run(Dictionary<string, string> lookup)
        {
            using var scm = new ServiceControlManager();
            return scm.Shutdown(ServiceName);
        }
    }
}
