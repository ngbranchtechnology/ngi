﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Xml;
using System.Collections.Generic;
using System.Diagnostics;
using Serilog;
using ngbt.common;

namespace ngi.implementation.Tasks
{
    public class KillProcessTask : ITask
    {
        public readonly ILogger Log;
        public readonly string Name;

        public KillProcessTask(string name)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            Name = name;
        }
        public void WriteInstallationXml(XmlWriter writer, Dictionary<string, string> lookup)
        {
            writer.WriteStartElement("KillProcessTask");
            writer.WriteAttributeString("Name", Name);
            writer.WriteEndElement(); // KillProcessTask
        }

        public static ITask FromXml(XmlReader reader)
        {
            return new KillProcessTask(
                name: reader.GetAttribute("Name")
            );
        }

        public bool Run(Dictionary<string, string> lookup)
        {
            string name = Name;
            if(name.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
            {
                name = name[..^4];
            }
            foreach (var process in Process.GetProcessesByName(Name))
            {
                Log.Information("Kill {0} [matches {1}]", process, Name);
                process.Kill();
            }
            return true;
        }
    }
}
