﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.IO;
using Serilog;
using System.Text.RegularExpressions;
using System.Xml;
using System.Security.AccessControl;
using System.Security.Principal;
using ngbt.common;
using System.Runtime.Versioning;

namespace ngi.implementation.Instructions
{
    [SupportedOSPlatform("windows")]
    public class CopyFilesInstruction : IInstruction
    {
        public readonly ILogger Log;
        public readonly string SourcePath;
        public readonly string ArchivePath;
        public readonly bool Recursive;
        public readonly string IncludePattern;
        public readonly string ExcludePattern;
        public readonly HashSet<string> CreatedDirectories = new HashSet<string>();

        public CopyFilesInstruction(
            string sourcePath,
            string archivePath,
            bool recursive,
            string includePattern = null,
            string excludePattern = null)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            SourcePath = sourcePath;
            ArchivePath = archivePath;
            Recursive = recursive;
            IncludePattern = includePattern;
            ExcludePattern = excludePattern;
        }

        public override string ToString()
        {
            return $"CopyFilesInstruction({SourcePath})";
        }

        public void WriteInstallationXml(XmlWriter writer, Dictionary<string, string> lookup)
        {
            writer.WriteStartElement("CopyFilesInstruction");

            writer.WriteAttributeString("SourcePath", Tools.ShortenName(SourcePath, lookup));
            writer.WriteAttributeString("ArchivePath", ArchivePath);
            writer.WriteAttributeString("Recursive", Recursive.ToString());

            if (!string.IsNullOrEmpty(IncludePattern))
            {
                writer.WriteAttributeString("IncludePattern", IncludePattern);
            }
            if (!string.IsNullOrEmpty(ExcludePattern))
            {
                writer.WriteAttributeString("ExcludePattern", ExcludePattern);
            }
            writer.WriteEndElement(); // CopyFilesInstruction
        }

        public static IInstruction FromXml(XmlReader reader)
        {
            return new CopyFilesInstruction(
                sourcePath: reader.GetAttribute("SourcePath"),
                archivePath: reader.GetAttribute("ArchivePath"),
                recursive: reader.GetBooleanAttribute("Recursive", defaultValue: true),
                includePattern: reader.GetOptionalAttributeString("IncludePattern"),
                excludePattern: reader.GetOptionalAttributeString("ExcludePattern")
            );
        }

        public bool Uninstall(Dictionary<string, string> lookup)
        {
            var targetName = Tools.ExpandName(SourcePath, lookup);
            Log.Information("- Removing all files in {0}", targetName);
            var result = Tools.DeleteFilesRecursive(targetName);
            return result;
        }

        public bool Restore(IArchiveReader archive, Dictionary<string, string> lookup)
        {
            bool first = true;
            int n = ArchivePath.Length;
            foreach (string archiveName in archive.GetMatchingEntries(ArchivePath))
            {
                string targetName = Tools.ExpandName(SourcePath, lookup);
                if(first)
                {
                    Log.Information("- Restoring {0}", targetName);
                    first = false;
                }
                var substring = archiveName[n..];

                // this is a folder-only, ignore it
                if (substring.EndsWith('/'))
                    continue;

                substring = substring.Replace('/', '\\');
                if (substring.StartsWith('\\'))
                    substring = substring[1..];
                if (string.IsNullOrEmpty(substring))
                    continue;
                targetName = Path.Combine(targetName, substring);
                string directory = Path.GetDirectoryName(targetName);
                string key = directory.ToLower();
                if (!CreatedDirectories.Contains(key))
                {
                    try
                    {
                        var sec = new DirectorySecurity();
                        var everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                        sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));

                        FileSystemAclExtensions.CreateDirectory(sec, directory);
                        CreatedDirectories.Add(key);
                        Log.Information("--- {0}", directory);
                    }
                    catch (Exception e)
                    {
                        Log.Fatal(e, "Unable to create directory {0}", directory);
                        return false;
                    }
                }
                if(!archive.ReadLocalFile(
                    archiveName: archiveName,
                    localName: targetName))
                {
                    return false;
                }
            }
            return true;
        }

        public bool Backup(IArchiveWriter writer, Dictionary<string, string> lookup)
        {
            var eo = new EnumerationOptions()
            {
                IgnoreInaccessible = true,
                RecurseSubdirectories = Recursive
            };

            string excludePattern = null;
            if(!string.IsNullOrEmpty(ExcludePattern))
            {
                excludePattern = Tools.WildCardToRegular(ExcludePattern);
            }

            string expandedSourcePath = Tools.ExpandName(SourcePath, lookup);
            Log.Information("- Exporting {0}\r\n         to {1}", expandedSourcePath, ArchivePath);
            int n = expandedSourcePath.Length;
            if(!expandedSourcePath.EndsWith('\\'))
            {
                n += 1;
            }
            foreach (string sourcePathAndFileName in Directory.EnumerateFiles(
                expandedSourcePath,
                string.IsNullOrEmpty(IncludePattern) ? "*" : IncludePattern,
                eo))
            {
                if (!string.IsNullOrEmpty(excludePattern) &&
                    Regex.IsMatch(Path.GetFileName(sourcePathAndFileName), excludePattern))
                {
                    continue;
                }

                string targetPathName = Path.Combine(
                    ArchivePath,
                    sourcePathAndFileName[n..]
                );

                if(!writer.WriteLocalFile(sourcePathAndFileName, targetPathName))
                {
                    break;
                }
            }
            return true;
        }
    }
}
