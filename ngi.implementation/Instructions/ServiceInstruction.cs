﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System.Collections.Generic;
using Serilog;
using System.IO;
using ngbt.win32.services;
using System.Runtime.Versioning;
using System.Xml;
using ngbt.common;
using ngbt.win32.registry;
using System;

namespace ngi.implementation.Instructions
{
    [SupportedOSPlatform("windows")]
    public class ServiceInstruction : IInstruction
    {
        private readonly ILogger Log;
        public readonly string ServiceName;
        public readonly string ArchivePath;

        public ServiceInstruction(
            string serviceName,
            string archivePath)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            ServiceName = serviceName;
            ArchivePath = archivePath;
        }

        public static IInstruction FromXml(XmlReader reader)
        {
            return new ServiceInstruction(
                serviceName: reader.GetAttribute("ServiceName"),
                archivePath: reader.GetAttribute("ArchivePath")
            );
        }

        public override string ToString()
        {
            return $"ServiceInstruction({ServiceName})";
        }

        public void WriteInstallationXml(XmlWriter writer, Dictionary<string, string> lookup)
        {
            writer.WriteStartElement("ServiceInstruction");
            writer.WriteAttributeString("ServiceName", ServiceName);
            writer.WriteAttributeString("ArchivePath", ArchivePath);
            writer.WriteEndElement(); // ServiceInstruction
        }

        public bool Uninstall(Dictionary<string, string> lookup)
        {
            using var scm = new ServiceControlManager();
            return scm.Uninstall(ServiceName);
        }

        public bool Restore(IArchiveReader archive, Dictionary<string, string> lookup)
        {
            Log.Information("- Restoring {0}", ArchivePath);

            string content = archive.ReadString(archiveName: ArchivePath);

            var ri = RegFile.CreateImporterFromString(content);
            var rke = ri.Import();
            while (rke.Keys.Count > 0)
            {
                foreach (var key in rke.Keys)
                {
                    rke = key.Value;
                    break;
                }
            }
            string lpDisplayName = rke.Values["displayname"].Value as string;
            string lpServiceName = Path.GetFileNameWithoutExtension(ArchivePath);
            string lpBinaryPathName = rke.Values["imagepath"].Value as string;
            uint dwServiceType = AsSafeInteger(rke.Values["type"].Value, (uint)SC_SERVICE_TYPE.SERVICE_WIN32_OWN_PROCESS);
            uint dwStartType = AsSafeInteger(rke.Values["start"].Value, (uint)SC_START_TYPE.SERVICE_DEMAND_START);
            uint dwErrorControl = AsSafeInteger(rke.Values["errorcontrol"].Value, (int)SC_ERROR_CONTROL.SERVICE_ERROR_IGNORE);

            using (var scm = new ServiceControlManager((uint)SCM_ACCESS.SC_MANAGER_ALL_ACCESS))
            {
                scm.CreateService(lpServiceName, lpDisplayName, dwServiceType, dwStartType, dwErrorControl, lpBinaryPathName);
            }
            return true;
        }

        private static uint AsSafeInteger(object value, uint defaultValue)
        {
            if(value is uint uintValue)
            {
                return uintValue;
            }
            if (value is int intValue)
            {
                return Convert.ToUInt32(intValue);
            }
            return defaultValue;
        }

        public bool Backup(IArchiveWriter archive, Dictionary<string, string> lookup)
        {
            return RegistryInstruction.Backup(Log, archive, lookup,
                $"HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\{ServiceName}",
                ArchivePath);
        }
    }
}
