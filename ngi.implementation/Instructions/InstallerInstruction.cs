﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using ngbt.win32.registry;
using ngbt.common;
using System.Runtime.Versioning;

namespace ngi.implementation.Instructions
{
    [SupportedOSPlatform("windows")]
    public static class InstallerInstruction 
    {
        public static bool Detect(string installerName, out RegistryInstruction result)
        {
            var installerKeys = new string[] {
                @"HKEY_CLASSES_ROOT\Installer\Products",
                @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Installer",
                @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Installer",
                @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
            };

            foreach (var installerKey in installerKeys)
            {
                using var root = Regis3.OpenRegistryHive(installerKey, out string subKeyName, Microsoft.Win32.RegistryView.Registry32);
                foreach (var subkey in root.FindString(subKeyName, installerName))
                {
                    Tools.StaticLogger.Information("Found subkey {0}", subkey);
                    if (subkey.EndsWith("\\ProductName", StringComparison.OrdinalIgnoreCase))
                    {
                        var tokens = subkey.Split('\\');
                        var token = tokens[^2];
                        var keyRemainder = subkey[(subKeyName.Length + 1)..^12];

                        var exportKey = $"{installerKey}\\{keyRemainder}";

                        result = new RegistryInstruction(
                            exportKey,
                            $"Installer\\{token}.txt");
                        return true;
                    }
                    else if(subkey.StartsWith(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"))
                    {
                        var tokens = subkey.Split('\\');
                        var token = tokens[^2];
                        var exportKey = $"{installerKey}\\{token}";
                        result = new RegistryInstruction(
                            exportKey,
                            $"Installer\\{token}.txt");
                        return true;
                    }
                    else
                    {
                        Tools.StaticLogger.Warning("Found key {0}, but don't know how to process it further...", subkey);
                    }
                }
            }
            result = null;
            return false;
        }
    }
}
