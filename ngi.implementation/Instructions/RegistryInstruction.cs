﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.IO;
using Serilog;
using ngbt.win32.registry;
using Microsoft.Win32;
using System.Runtime.Versioning;
using System.Xml;
using ngbt.common;

namespace ngi.implementation.Instructions
{
    [SupportedOSPlatform("windows")]
    public class RegistryInstruction : IInstruction
    {
        protected readonly ILogger Log;
        protected readonly string KeyName;
        protected readonly string ArchivePath;

        public RegistryInstruction(
            string keyName,
            string archivePath)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());
            KeyName = keyName;
            ArchivePath = archivePath;
        }

        public virtual void WriteInstallationXml(XmlWriter writer, Dictionary<string, string> lookup)
        {
            writer.WriteStartElement("RegistryInstruction");
            writer.WriteAttributeString("KeyName", KeyName);
            writer.WriteAttributeString("ArchivePath", ArchivePath);
            writer.WriteEndElement(); // RegistryInstruction
        }

        public static IInstruction FromXml(XmlReader reader)
        {
            return new RegistryInstruction(
                keyName: reader.GetAttribute("KeyName"),
                archivePath: reader.GetAttribute("ArchivePath")
            );
        }

        public virtual bool Uninstall(Dictionary<string, string> lookup)
        {
            Log.Information("- Removing {0}", KeyName);
            using var root = Regis3.OpenRegistryHive(KeyName, out string subKeyName, RegistryView.Registry32);
            foreach (var keyUnableToDelete in root.DeleteKeyRecursive(subKeyName))
            {
                Log.Information("- Unable to remove {0}", keyUnableToDelete);
            }
            return true;
        }

        public virtual bool Restore(IArchiveReader archive, Dictionary<string, string> lookup)
        {
            Log.Information("- Restoring {0}", ArchivePath);
            try
            {
                string content = archive.ReadString(archiveName: ArchivePath);

                var rer = new RegEnvReplace();
                if (lookup != null)
                {
                    // we are RESTORING data, so the input is a dictionary
                    // of the form $$INSTDIR$$ = ...some location

                    foreach (var replaceThis in lookup.Keys)
                    {
                        var replaceWithThat = lookup[replaceThis];

                        rer.Variables[replaceThis.Replace("$$", "")] = Tools.ExpandName(replaceWithThat);
                    }
                }
                var key = RegFile.CreateImporterFromString(content, RegFileImportOptions.None).Import();
                key.WriteToTheRegistry(RegistryWriteOptions.AllAccessForEveryone | RegistryWriteOptions.Recursive, rer, RegistryView.Registry32);
                return true;
            }
            catch (Exception e)
            {
                if (ArchivePath.ToLower().Contains("wosaxfs.txt"))
                    return true;
                Log.Fatal(e, "Unable to restore {0}", ArchivePath);
                return false;
            }
        }

        public virtual bool Backup(IArchiveWriter archive, Dictionary<string, string> lookup)
        {
            return Backup(Log, archive, lookup, KeyName, ArchivePath);
        }

        public static bool Backup(ILogger log, IArchiveWriter archive, Dictionary<string, string> lookup, string keyName, string archivePath)
        {
            log.Information("- Exporting {0}\r\n         to {1}", keyName, archivePath);
            using (var rootKey = Regis3.OpenRegistryHive(keyName, out string rootPathWithoutHive, RegistryView.Registry32))
            {
                var entry = new RegistryImporter(rootKey, rootPathWithoutHive).Import();
                using var sw = new StringWriter();
                var exporter = new RegFileFormat4Exporter();
                exporter.SetReplacementLookup(lookup.Reverse());
                exporter.Export(entry, sw, RegFileExportOptions.NoEmptyKeys);

                archive.WriteString(sw.ToString(), archivePath);
            }
            return true;
        }
    }
}
